const inputNewToDo = document.getElementById('inputToDo');
const inputJsonFile = document.getElementById('inputJsonFile');

// Counter um die einzelnen ToDos zu nummerieren
let toDoNumber = 0;

// Auto-select input field on page load (difference focus vs. select? Different browsers?)
inputNewToDo.focus();
inputNewToDo.select();

// load To Dos from local storage
if (window.localStorage.getItem('savedToDos') != null) {
  readToDosFromJson(window.localStorage.getItem('savedToDos'));
};

// create new list element, is called by addElement button w/ text input as value and on loading page w/ saved To Dos as input
function addElementToList(inputText) {
  toDoNumber++;

  // create new list element, textbox, text for it, and append text to textbox and textbox to element
  // textbox is necessary, so that edit button will not make buttons editable
  const newListElement = document.createElement("li");
  const newListElementTextbox = document.createElement("p");
  newListElementTextbox.setAttribute("class", "listTextbox");
  const newListElementText = document.createTextNode(inputText);
  newListElement.appendChild(newListElementTextbox);
  newListElementTextbox.appendChild(newListElementText);

  //create delete button, adds properties and appends it to list element
  const newListElementDeleteButton = document.createElement("button");
  newListElementDeleteButton.setAttribute("class", "delete");
  newListElementDeleteButton.setAttribute("onclick", "deleteListItem(this.parentElement.id)");
  const newListElementDeleteButtonText = document.createTextNode("x");
  newListElementDeleteButton.appendChild(newListElementDeleteButtonText);
  newListElement.appendChild(newListElementDeleteButton);

  //create edit button, adds properties and appends it to list element
  const newListElementEditButton = document.createElement("button");
  newListElementEditButton.setAttribute("class", "edit");
  newListElementEditButton.setAttribute("onclick", "editListItem(this.parentElement.id)");
  const newListElementEditButtonText = document.createTextNode("edit");
  newListElementEditButton.appendChild(newListElementEditButtonText);
  newListElement.appendChild(newListElementEditButton);

  // add class, id and onclick (call crossOutToDo()) to list element
  newListElement.setAttribute("class", "toDoItem notCrossedOut");
  newListElement.setAttribute("id", toDoNumber);
  newListElement.setAttribute("onclick", "crossOutToDo(id)");

  // add new list element to list
  document.getElementById('outputList').appendChild(newListElement);

  saveToStorage();
};

// cross out/un-cross out a To Do List element on clicking it, is called by list item
function crossOutToDo(idListItem) {
  const toCrossOut = document.getElementById(idListItem);
  // first "if" prevents (un)crossing if text is being edited
  if (toCrossOut.childNodes[0].getAttribute("contenteditable") === "true") {
    return
  } else if (toCrossOut.classList.contains("notCrossedOut")) {
    toCrossOut.className = "toDoItem crossedOut";
  } else {
    toCrossOut.className = "toDoItem notCrossedOut";
  };

  saveToStorage();
};

// delete list item, is called by delete button
function deleteListItem(idListElement) {
  // prevent Uncaught TypeError by canceling crossOutToDo(). Hope it doesn't mess anyting up.
  event.stopPropagation();
  const allToDos = document.getElementById("outputList");
  allToDos.removeChild(document.getElementById(idListElement));

  saveToStorage();
};

// edit list item, is called by edit button
function editListItem(idListElement) {
  // prevent crossOutToDo(). Hope it doesn't mess anyting up.
  event.stopPropagation();
  const listElement = document.getElementById(idListElement);
  const textboxListElement = listElement.childNodes[0];

  // add editable to class name to change cursor, make content of textbox editable
  textboxListElement.className = "listTextbox editable";
  textboxListElement.setAttribute("contenteditable", "true");
  textboxListElement.focus();
  // TODO: cursor ans Ende des Texts bewegen. Ist wohl komplizierter.

  // remove contenteditable and change class back after textboxListElement is no longer selected and save to storage
  textboxListElement.addEventListener("blur", function(event) {
    textboxListElement.setAttribute("contenteditable", "false");
    textboxListElement.className = "listTextbox";
    saveToStorage();
  });

  // finish edit w/ enter; keydown instead of keyup to prevent input of enter
  textboxListElement.addEventListener("keydown", function(event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
      // Cancel the default action, if needed
      event.preventDefault();
      // Trigger the button element with a click
      textboxListElement.blur();
    }
  });
};

// Execute a function when the user releases a key on the keyboard
inputNewToDo.addEventListener("keyup", function(event) {
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    document.getElementById("addElement").click();
  }
});

// save To Dos as JSON file and return it, called by export button and by saveToStorage()
function saveAsJson() {
  // store To Dos as list of objects, each with text of To Do as key and notCrossedOut or crossedOut as value
  const listToDos = Array.from(document.getElementsByClassName("toDoItem")).map(item => {
    let newObject = {};
    newObject[item.firstChild.innerText] =  item.classList[1];
    return newObject
  });

  // convert list of To Dos to JSON object, store it in variable and return it
  let jsonToDos = JSON.stringify(listToDos);
  return jsonToDos;
};

// store all To Dos in local storage, is called by addElementToList(), deleteListItem(), editListItem() and crossOutToDo(), also by readToDosFromJson() if an item is crossedOut
// outsourced in function so it can be called by delete and edit buttons
function saveToStorage() {
  window.localStorage.setItem("savedToDos", saveAsJson());
};

// delete (after asking for confirmation) all To Dos and clear local Storage, called by deleteAll button
function deleteAll() {
  let toDelete = document.getElementsByClassName("toDoItem");
  const confirmDelete = confirm('Do you really want to delete all To Dos permanently?');
  if (confirmDelete === true) {
    while (toDelete[0]) {
      toDelete[0].parentNode.removeChild(toDelete[0]);
    };
    window.localStorage.clear();
  };
};

// download JSON file of all To Dos, is called by exportToDos-Button
function exportList() {
  jsonToDos = saveAsJson();

  // Muss ich jetzt vllt einfach mal nicht verstehen: https://en.wikipedia.org/wiki/Data_URI_scheme
  let dataUri = 'data:application/json;charset=utf-8,'+ encodeURIComponent(jsonToDos);

  let exportFileDefaultName = 'ToDos.json';

  // file download: create a link ('a'), add what to download (href), download function w/ file name, and click the link
  let linkElement = document.createElement('a');
  linkElement.setAttribute('href', dataUri);
  linkElement.setAttribute('download', exportFileDefaultName);
  document.body.appendChild(linkElement); // required for firefox
  linkElement.click();
};

// open popup window for JSON import, close login form first
function openPopup() {
  closeLoginForm();
  document.getElementById("popup").style.display = "block";
};

// close popup window for JSON import; delete potential error message
function closePopup() {
  document.getElementById("popup").style.display = "none";
  document.getElementById('errorOutput').innerHTML = ""
};

// import To Dos from JSON file; called by importToDos button
function importToDos() {
  const errorMessageNode = document.getElementById('errorOutput');
  errorMessageNode.innerHTML = "";

  const reader = new FileReader();

  try {
    if (inputJsonFile.files[0] === undefined) throw "No file selected!";
    window.onerror = () => {errorMessageNode.innerHTML = "Wrong type of file!"}

    reader.readAsText(inputJsonFile.files[0]);
    reader.onload = function() {
      readToDosFromJson(reader.result);
    };
  }
  catch(err) {
      errorMessageNode.innerHTML = err;
  };

  // TODO: bei error handler alle Fälle durchdenken
};

// read To Dos from JSON file, convert them to javascript object (or list?), call addElementToList() for each and cross out if necessary; called on page loading if file in local storage and by import button via importToDos()
function readToDosFromJson(jsonFile) {
  JSON.parse(jsonFile).forEach(item => {
    addElementToList(Object.keys(item)[0]);
    if (Object.values(item)[0] === "crossedOut") {
      document.getElementById('outputList').lastChild.className = "toDoItem crossedOut";
      saveToStorage(); // to avoid failure to save crossed-out state if last element is crossed out
    };
  });
};

// open login form, close import popup first
function openLoginForm() {
  closePopup();
  document.getElementById("login-form").style.display = "block";
};

// close login form (and new user container)
function closeLoginForm() {
  document.getElementById("login-form").style.display = "none";
  document.getElementById("newUser-container").style.display = "none";
};

// open new user container in form
function openNewUser() {
  document.getElementById("newUser-container").style.display = "block";
};

// TODO: Login-Gerüst mit content füllen. In Python selbst schreiben oder doch Django?
// TODO: bei verkleinertem Fenster rutscht der edit-button nach unten und das list element ändert die Höhe -> fixen?
// TODO: Undo falls To Do versehentlich gelöscht wurde
// TODO: Datenbank hinterlegen (Postgres, SQLite (einfacher)); Filter einbauen
// TODO: Python-Server für irgendetwas nutzen (Upload, Login...)
// TODO: Reihenfolge der list items per Drag&Drop verändern
// TODO: Delete-Button weniger dämlich machen?
